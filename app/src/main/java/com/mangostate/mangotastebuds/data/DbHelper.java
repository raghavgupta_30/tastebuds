package com.mangostate.mangotastebuds.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "mangotastebuds.db";

    private static final String TEXT_TYPE = " TEXT";

    private static final String INTEGER_TYPE = " INTEGER";

    private static final String BOOLEAN_TYPE = " INTEGER";

    private static final String COMMA_SEP = ",";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + RestaurantContract.RestaurantEntry.TABLE_NAME + " (" +
                    RestaurantContract.RestaurantEntry._ID + INTEGER_TYPE + " PRIMARY KEY," +
                    RestaurantContract.RestaurantEntry.COLUMN_NAME_RES_ID + TEXT_TYPE + COMMA_SEP +
                    RestaurantContract.RestaurantEntry.COLUMN_NAME_RES_NAME + TEXT_TYPE + COMMA_SEP +
                    RestaurantContract.RestaurantEntry.COLUMN_NAME_ADDRESS + TEXT_TYPE + COMMA_SEP +
                    RestaurantContract.RestaurantEntry.COLUMN_NAME_CITY + TEXT_TYPE + COMMA_SEP +
                    RestaurantContract.RestaurantEntry.COLUMN_NAME_LOCALITY + TEXT_TYPE + COMMA_SEP +
                    RestaurantContract.RestaurantEntry.COLUMN_NAME_LATITUDE + TEXT_TYPE + COMMA_SEP +
                    RestaurantContract.RestaurantEntry.COLUMN_NAME_LONGITUDE + TEXT_TYPE + COMMA_SEP +
                    RestaurantContract.RestaurantEntry.COLUMN_NAME_CUISINES + TEXT_TYPE + COMMA_SEP +
                    RestaurantContract.RestaurantEntry.COLUMN_NAME_COST_CURRENCY + TEXT_TYPE + COMMA_SEP +
                    RestaurantContract.RestaurantEntry.COLUMN_NAME_COST + TEXT_TYPE + COMMA_SEP +
                    RestaurantContract.RestaurantEntry.COLUMN_NAME_PHOTO_URL + TEXT_TYPE + COMMA_SEP +
                    RestaurantContract.RestaurantEntry.COLUMN_NAME_MENU_URL + TEXT_TYPE + COMMA_SEP +
                    RestaurantContract.RestaurantEntry.COLUMN_NAME_FEATURED_IMAGE_URL + TEXT_TYPE + COMMA_SEP +
                    RestaurantContract.RestaurantEntry.COLUMN_NAME_ONLINE_DELIVERY + BOOLEAN_TYPE + COMMA_SEP +
                    RestaurantContract.RestaurantEntry.COLUMN_NAME_THUMB_URL + TEXT_TYPE + COMMA_SEP +
                    RestaurantContract.RestaurantEntry.COLUMN_NAME_RATING_VALUE + TEXT_TYPE + COMMA_SEP +
                    RestaurantContract.RestaurantEntry.COLUMN_NAME_RATING_COLOR + TEXT_TYPE + COMMA_SEP +
                    RestaurantContract.RestaurantEntry.COLUMN_NAME_UPVOTES + INTEGER_TYPE +
                    " )";

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + RestaurantContract.RestaurantEntry.TABLE_NAME);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}

