package com.mangostate.mangotastebuds;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.mangostate.mangotastebuds.home.HomeView;
import com.mangostate.mangotastebuds.location.FetchLocation;
import com.mangostate.mangotastebuds.utility.PreferenceUtility;

public class Welcome extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        final PreferenceUtility mPref = new PreferenceUtility(getApplicationContext());

        // To Start Either Fetch Location or Home Activity
        int SPLASH_TIME_OUT = 3000;
        new Handler().postDelayed(new Runnable() {
                                      @Override
                                      public void run() {
                                          if (mPref.isLocation()) {
                                              //Calling Home Activity
                                              Intent i = new Intent(getApplicationContext(), HomeView.class);
                                              startActivity(i);
                                              finish();
                                          } else {
                                              //Calling Fetch Location Activity
                                              Intent i = new Intent(getApplicationContext(), FetchLocation.class);
                                              startActivity(i);
                                              finish();
                                          }
                                      }
                                  }

                , SPLASH_TIME_OUT);
    }
}

