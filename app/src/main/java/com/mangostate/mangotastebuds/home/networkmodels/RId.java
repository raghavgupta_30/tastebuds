package com.mangostate.mangotastebuds.home.networkmodels;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RId {
    @SerializedName("res_id")
    @Expose
    private Integer resId;

    public Integer getResId() {
        return resId;
    }

}
