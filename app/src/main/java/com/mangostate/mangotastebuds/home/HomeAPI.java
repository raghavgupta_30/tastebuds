package com.mangostate.mangotastebuds.home;

import com.mangostate.mangotastebuds.home.networkmodels.GeoLocationResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

interface HomeAPI {
    @GET("v2.1/search")
    Call<GeoLocationResponse> getLocationDetail(@Query("count") Integer count, @Query("lat") Double latitude, @Query("lon") Double longitude, @Query("radius") Double radius, @Query("sort") String sort);

}
