package com.mangostate.mangotastebuds.location;


import com.mangostate.mangotastebuds.location.networkmodels.LocationResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface LocationAPI {
    @GET("v2.1/locations")
    Call<LocationResponse> getLocations(@Query("query") String query);
}
