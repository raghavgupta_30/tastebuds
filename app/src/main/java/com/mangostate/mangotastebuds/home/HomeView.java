package com.mangostate.mangotastebuds.home;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mangostate.mangotastebuds.R;
import com.mangostate.mangotastebuds.data.RestaurantContract;
import com.mangostate.mangotastebuds.data.RestaurantDataSource;
import com.mangostate.mangotastebuds.data.RestaurantValues;
import com.mangostate.mangotastebuds.home.networkmodels.AllRestaurants;
import com.mangostate.mangotastebuds.home.networkmodels.GeoLocationResponse;
import com.mangostate.mangotastebuds.home.networkmodels.Restaurant;
import com.mangostate.mangotastebuds.home.recycler.RecyclerClickListener;
import com.mangostate.mangotastebuds.home.recycler.RecyclerTouchListener;
import com.mangostate.mangotastebuds.home.recycler.RestaurantsAdapter;
import com.mangostate.mangotastebuds.location.FetchLocation;
import com.mangostate.mangotastebuds.restaurantdetail.RestaurantDetailView;
import com.mangostate.mangotastebuds.utility.AppConstants;
import com.mangostate.mangotastebuds.utility.PreferenceUtility;
import com.mangostate.mangotastebuds.utility.ServiceGenerator;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeView extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String TAG = HomeView.class.getSimpleName();
    private static final int LOADER_ID = 1;

    private RestaurantsAdapter mRestaurantAdapter;
    private List<Restaurant> restaurantList = new ArrayList<>();
    private GeoLocationResponse geoLocationResponse;
    private PreferenceUtility mPref;
    private String mCount = "40";
    private String mRadius = "2000";
    private String mLatitude;
    private String mLongitude;
    private String[] mSort = {"rating", "cost", "real-distance"};
    private boolean mIsFabOpen = false;
    private boolean mAllowFabClick = true;

    //UI
    private CoordinatorLayout mCoordinator;
    private Snackbar mSnackBar;
    private ProgressBar mProgressBar;
    private RecyclerView mRestaurantView;
    private LinearLayout mLocationView;
    private TextView mLocationName;
    private LinearLayout mNoResultsFound;
    private RelativeLayout mHomeView, mFilterView;
    private FloatingActionButton mFab, mRatings, mCost, mDistance;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //Shared Preferences
        mPref = new PreferenceUtility(getApplicationContext());

        //Initialise UI Views
        initialiseViews();

        if (mPref.isLocation()) {
            mLatitude = mPref.getCurrentLat();
            mLongitude = mPref.getCurrentLon();
            //Unless Search is again Required
            mPref.setDoSearch(false);
            //Getting Data from Local DB if data not available then from Server
            showProgress(true);
            getSupportLoaderManager().initLoader(LOADER_ID, null, this);
        } else {
            sendToast(getString(R.string.select_location));
            showLocation();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mPref.isDoSearch()) {
            mLatitude = mPref.getCurrentLat();
            mLongitude = mPref.getCurrentLon();
            //Unless Search is again Required
            mPref.setDoSearch(false);
            //Getting Data from Local DB if data not available then from Server
            showProgress(true);
            getSupportLoaderManager().restartLoader(LOADER_ID, null, HomeView.this);
        }
    }

    private void showLocation() {
        Intent iLocation = new Intent(HomeView.this, FetchLocation.class);
        startActivity(iLocation);
    }

    private void initialiseViews() {
        mCoordinator = (CoordinatorLayout) findViewById(R.id.home_coordinator);
        mHomeView = (RelativeLayout) findViewById(R.id.home_view);
        mFilterView = (RelativeLayout) findViewById(R.id.filter_view);
        mRestaurantView = (RecyclerView) findViewById(R.id.home_restaurant);
        mProgressBar = (ProgressBar) findViewById(R.id.home_progress);
        mLocationView = (LinearLayout) findViewById(R.id.home_location_view);
        mLocationName = (TextView) findViewById(R.id.home_location_name);
        mNoResultsFound = (LinearLayout) findViewById(R.id.no_result);
        mFab = (FloatingActionButton) findViewById(R.id.fab);
        mRatings = (FloatingActionButton) findViewById(R.id.ratings);
        mCost = (FloatingActionButton) findViewById(R.id.cost);
        mDistance = (FloatingActionButton) findViewById(R.id.distance);
        //Filter View OnClick
        mFilterView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleView();
            }
        });

        //Floating ActionButton OnClick Listeners
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mAllowFabClick)
                    toggleView();
            }
        });

        mDistance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleView();
                getDataFromZomato(mLatitude, mLongitude, mCount, mRadius, mSort[2]);
            }
        });

        mCost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleView();
                getDataFromZomato(mLatitude, mLongitude, mCount, mRadius, mSort[1]);
            }
        });

        mRatings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleView();
                getDataFromZomato(mLatitude, mLongitude, mCount, mRadius, mSort[0]);
            }
        });

        //Setting Recycler Adapter Value
        mRestaurantAdapter = new RestaurantsAdapter(HomeView.this, restaurantList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRestaurantView.setLayoutManager(mLayoutManager);
        mRestaurantView.setItemAnimator(new DefaultItemAnimator());
        mRestaurantView.setAdapter(mRestaurantAdapter);

        mLocationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLocation();
            }
        });
        mRestaurantView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mRestaurantView, new RecyclerClickListener() {
            @Override
            public void onClick(View view, int position) {
                Restaurant restaurant = restaurantList.get(position);
                showDetailRestaurant(restaurant);
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
    }

    private void toggleView() {
        if (mIsFabOpen) {
            mFilterView.setVisibility(View.GONE);
            mIsFabOpen = false;
        } else {
            mFilterView.setVisibility(View.VISIBLE);
            mIsFabOpen = true;
        }
    }

    private void showDetailRestaurant(Restaurant restaurant) {
        Intent iDetailRestaurant = new Intent(HomeView.this, RestaurantDetailView.class);
        iDetailRestaurant.putExtra(AppConstants.KEY_RESTAURANT_ID, restaurant.getId());
        startActivity(iDetailRestaurant);
    }


    /*-----------------------------------------------------------FETCHING DATA FROM ZOMATO API'S-----------------------------------------------------------------*/

    private void getDataFromZomato(String lat, String lon, String count, String radius, String order) {
        HomeAPI homeService = ServiceGenerator.createService(HomeAPI.class);
        Call<GeoLocationResponse> locationCall;
        showProgress(true);

        //Calling the API Call Object with Callbacks : Success & Failure
        locationCall = homeService.getLocationDetail(Integer.valueOf(count), Double.valueOf(lat), Double.valueOf(lon), Double.valueOf(radius), order);
        locationCall.enqueue(new Callback<GeoLocationResponse>() {
            @Override
            public void onResponse(Call<GeoLocationResponse> call, Response<GeoLocationResponse> response) {
                if (response.isSuccessful()) {
                    geoLocationResponse = response.body();
                    if (geoLocationResponse.getResultsFound() > 0) {
                        showNoResultsFound(false);
                        saveInDb(geoLocationResponse);
                    } else {
                        showProgress(false);
                        showNoResultsFound(true);
                    }
                } else {
                    Log.d(TAG, "error " + response.message());
                    showProgress(false);
                    sendSnack(AppConstants.HOME_ERROR);
                }
            }

            @Override
            public void onFailure(Call<GeoLocationResponse> call, Throwable t) {
                Log.d(TAG, "error " + t.getMessage());
                showProgress(false);
                sendSnack(AppConstants.HOME_ERROR);
            }
        });
    }

    private void showNoResultsFound(boolean inBoolShow) {
        mNoResultsFound.setVisibility(inBoolShow ? View.VISIBLE : View.GONE);
        mRestaurantView.setVisibility(inBoolShow ? View.GONE : View.VISIBLE);
        mFab.setVisibility(inBoolShow ? View.GONE : View.VISIBLE);
    }

    private void sendToast(String iToastMessage) {
        mSnackBar = Snackbar
                .make(mCoordinator, iToastMessage, Snackbar.LENGTH_LONG);
        //SnackBar On Dismiss Listener
        mSnackBar.setCallback(new Snackbar.Callback() {
            @Override
            public void onDismissed(Snackbar snackbar, int event) {
                super.onDismissed(snackbar, event);
                mAllowFabClick = true;
            }

            @Override
            public void onShown(Snackbar snackbar) {
                super.onShown(snackbar);
                mAllowFabClick = false;
            }
        });

        mSnackBar.show();
    }

    private void sendSnack(int iCode) {
        String msg;
        switch (AppConstants.HOME_ERROR) {
            //Home Error
            case AppConstants.HOME_ERROR:
                msg = getString(R.string.home_error);
                mSnackBar = Snackbar
                        .make(mCoordinator, msg, Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //Restart Cursor Loader
                                getSupportLoaderManager().restartLoader(LOADER_ID, null, HomeView.this);
                            }
                        });
                //SnackBar On Dismiss Listener
                mSnackBar.setCallback(new Snackbar.Callback() {
                    @Override
                    public void onDismissed(Snackbar snackbar, int event) {
                        super.onDismissed(snackbar, event);
                        mAllowFabClick = true;
                    }

                    @Override
                    public void onShown(Snackbar snackbar) {
                        super.onShown(snackbar);
                        mAllowFabClick = false;
                    }
                });

                mSnackBar.show();
                break;
        }
    }

    private void showProgress(final boolean inBoolShow) {
        mProgressBar.setVisibility(inBoolShow ? View.VISIBLE : View.GONE);
        mHomeView.setVisibility(inBoolShow ? View.GONE : View.VISIBLE);
        mFab.setVisibility(inBoolShow ? View.GONE : View.VISIBLE);
    }

    /*-----------------------------------------------------------------------------CONTENT PROVIDER STARTS------------------------------------------------------------------*/
    private void saveInDb(GeoLocationResponse geoLocationResponse) {
        try {
            //Creating instance of Restaurant Data source
            RestaurantDataSource ds = RestaurantDataSource.getInstance(getContentResolver());
            //To clear old data of Restaurants
            ds.deleteAllRestaurants();

            for (AllRestaurants resWrapper : geoLocationResponse.getRestaurants()) {
                ds.saveRestaurant(resWrapper.getRestaurant());

            }
            //Restart CursorLoader
            getSupportLoaderManager().restartLoader(LOADER_ID, null, HomeView.this);
        } catch (Exception ex) {
            showProgress(false);
            Log.d(TAG, "error " + ex.toString());
            sendSnack(AppConstants.HOME_ERROR);
        }
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(
                HomeView.this,
                RestaurantContract.RestaurantEntry.buildRestaurantsUri(),
                RestaurantContract.RestaurantEntry.RESTAURANT_COLUMNS, null, null, null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data == null || !data.moveToFirst()) {
            getDataFromZomato(mLatitude, mLongitude, mCount, mRadius, mSort[0]);
        } else {
            restaurantList.clear();
            while (!data.isAfterLast()) {
                restaurantList.add(RestaurantValues.from(data));
                data.moveToNext();
            }
            mRestaurantView.smoothScrollToPosition(0);
            mRestaurantAdapter.notifyDataSetChanged();

            //Setting the Selected Location Name
            if (mPref.getSelectedLocation() == null) {
                mLocationName.setText("");
            } else {
                mLocationName.setText(mPref.getSelectedLocation());
            }
            showProgress(false);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}