package com.mangostate.mangotastebuds.home.recycler;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mangostate.mangotastebuds.R;
import com.mangostate.mangotastebuds.home.networkmodels.Restaurant;
import com.mangostate.mangotastebuds.utility.RoundImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RestaurantsAdapter extends RecyclerView.Adapter<RestaurantsAdapter.MyViewHolder> {

    private List<Restaurant> restaurantList;
    private Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, cost, address, rating, cuisines;
        public RoundImageView restaurant_image;
        public CardView rating_background;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.restaurant_title);
            address = (TextView) view.findViewById(R.id.restaurant_address);
            cost = (TextView) view.findViewById(R.id.restaurant_cost);
            rating = (TextView) view.findViewById(R.id.restaurant_rating);
            cuisines = (TextView) view.findViewById(R.id.restaurant_cuisines);
            restaurant_image = (RoundImageView) view.findViewById(R.id.restaurant_image);
            rating_background=(CardView)view.findViewById(R.id.rating_background);
        }
    }

    public RestaurantsAdapter(Context context, List<Restaurant> restaurantList) {
        mContext = context;
        this.restaurantList = restaurantList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.restaurant_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Restaurant restaurant = restaurantList.get(position);
        holder.title.setText(restaurant.getName());
        String adresss = restaurant.getLocation().getLocality() + ", " + restaurant.getLocation().getCity();
        holder.address.setText(adresss);
        String cost = restaurant.getCurrency() + restaurant.getAverageCostForTwo() + " " + mContext.getResources().getString(R.string.cost_for_two);
        holder.cost.setText(cost);
        holder.cuisines.setText(restaurant.getCuisines());
        holder.rating.setText(restaurant.getUserRating().getAggregateRating());
        holder.rating_background.setCardBackgroundColor(Color.parseColor("#" + restaurant.getUserRating().getRatingColor()));

        //In case no Image is provided from Server
        String url = restaurant.getThumb();
        if (url == null || "".equals(url)) {
            Picasso.with(mContext)
                    .load(R.drawable.placeholder3)
                    .resize(200, 200)
                    .centerCrop()
                    .error(R.drawable.placeholder3)
                    .placeholder(R.drawable.placeholder3)
                    .into(holder.restaurant_image);

        } else {
            Picasso.with(mContext)
                    .load(restaurant.getThumb())
                    .resize(200, 200)
                    .centerCrop()
                    .error(R.drawable.placeholder3)
                    .placeholder(R.drawable.placeholder3)
                    .into(holder.restaurant_image);
        }
    }

    @Override
    public int getItemCount() {
        return restaurantList.size();
    }
}
