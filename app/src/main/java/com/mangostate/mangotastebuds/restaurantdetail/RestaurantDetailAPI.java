package com.mangostate.mangotastebuds.restaurantdetail;

import com.mangostate.mangotastebuds.home.networkmodels.Restaurant;
import com.mangostate.mangotastebuds.restaurantdetail.networkmodel.ReviewResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RestaurantDetailAPI {
    @GET("v2.1/restaurant")
    Call<Restaurant> getRestaurantDetail(@Query("res_id") String restaurantId);

    @GET("v2.1/reviews")
    Call<ReviewResponse> getReviewDetail(@Query("res_id") String restaurantId, @Query("count") Integer count);
}
