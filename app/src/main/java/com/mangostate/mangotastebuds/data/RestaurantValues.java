package com.mangostate.mangotastebuds.data;


import android.content.ContentValues;
import android.database.Cursor;

import com.mangostate.mangotastebuds.home.networkmodels.Restaurant;
import com.mangostate.mangotastebuds.home.networkmodels.RestaurantLocation;
import com.mangostate.mangotastebuds.home.networkmodels.UserRating;

public class RestaurantValues {


    public static ContentValues from(Restaurant restaurant) {
        ContentValues values = new ContentValues();
        values.put(RestaurantContract.RestaurantEntry.COLUMN_NAME_RES_ID, restaurant.getId());
        values.put(RestaurantContract.RestaurantEntry.COLUMN_NAME_RES_NAME, restaurant.getName());
        values.put(RestaurantContract.RestaurantEntry.COLUMN_NAME_ADDRESS, restaurant.getLocation().getAddress());
        values.put(RestaurantContract.RestaurantEntry.COLUMN_NAME_CITY, restaurant.getLocation().getCity());
        values.put(RestaurantContract.RestaurantEntry.COLUMN_NAME_LOCALITY, restaurant.getLocation().getLocality());
        values.put(RestaurantContract.RestaurantEntry.COLUMN_NAME_LATITUDE, restaurant.getLocation().getLatitude());
        values.put(RestaurantContract.RestaurantEntry.COLUMN_NAME_LONGITUDE, restaurant.getLocation().getLongitude());
        values.put(RestaurantContract.RestaurantEntry.COLUMN_NAME_CUISINES, restaurant.getCuisines());
        values.put(RestaurantContract.RestaurantEntry.COLUMN_NAME_COST_CURRENCY, restaurant.getCurrency());
        values.put(RestaurantContract.RestaurantEntry.COLUMN_NAME_COST, restaurant.getAverageCostForTwo());
        values.put(RestaurantContract.RestaurantEntry.COLUMN_NAME_PHOTO_URL, restaurant.getPhotosUrl());
        values.put(RestaurantContract.RestaurantEntry.COLUMN_NAME_MENU_URL, restaurant.getMenuUrl());
        values.put(RestaurantContract.RestaurantEntry.COLUMN_NAME_FEATURED_IMAGE_URL, restaurant.getFeaturedImage());
        values.put(RestaurantContract.RestaurantEntry.COLUMN_NAME_THUMB_URL, restaurant.getThumb());
        values.put(RestaurantContract.RestaurantEntry.COLUMN_NAME_ONLINE_DELIVERY, restaurant.getHasOnlineDelivery());
        values.put(RestaurantContract.RestaurantEntry.COLUMN_NAME_RATING_COLOR, restaurant.getUserRating().getRatingColor());
        values.put(RestaurantContract.RestaurantEntry.COLUMN_NAME_RATING_VALUE, restaurant.getUserRating().getAggregateRating());
        values.put(RestaurantContract.RestaurantEntry.COLUMN_NAME_UPVOTES, restaurant.getUserRating().getVotes());
        return values;
    }

    public static Restaurant from(Cursor cursor) {
        String resId = cursor.getString(cursor.getColumnIndexOrThrow(
                RestaurantContract.RestaurantEntry.COLUMN_NAME_RES_ID));
        String resName = cursor.getString(cursor.getColumnIndexOrThrow(
                RestaurantContract.RestaurantEntry.COLUMN_NAME_RES_NAME));
        String address = cursor.getString(cursor.getColumnIndexOrThrow(
                RestaurantContract.RestaurantEntry.COLUMN_NAME_ADDRESS));
        String city = cursor.getString(cursor.getColumnIndexOrThrow(
                RestaurantContract.RestaurantEntry.COLUMN_NAME_CITY));
        String locality = cursor.getString(cursor.getColumnIndexOrThrow(
                RestaurantContract.RestaurantEntry.COLUMN_NAME_LOCALITY));
        String latitude = cursor.getString(cursor.getColumnIndexOrThrow(
                RestaurantContract.RestaurantEntry.COLUMN_NAME_LATITUDE));
        String longitude = cursor.getString(cursor.getColumnIndexOrThrow(
                RestaurantContract.RestaurantEntry.COLUMN_NAME_LONGITUDE));
        String cost_currency = cursor.getString(cursor.getColumnIndexOrThrow(
                RestaurantContract.RestaurantEntry.COLUMN_NAME_COST_CURRENCY));
        String cost = cursor.getString(cursor.getColumnIndexOrThrow(
                RestaurantContract.RestaurantEntry.COLUMN_NAME_COST));
        String cuisines = cursor.getString(cursor.getColumnIndexOrThrow(
                RestaurantContract.RestaurantEntry.COLUMN_NAME_CUISINES));
        String feature_image = cursor.getString(cursor.getColumnIndexOrThrow(
                RestaurantContract.RestaurantEntry.COLUMN_NAME_FEATURED_IMAGE_URL));
        String menu_url = cursor.getString(cursor.getColumnIndexOrThrow(
                RestaurantContract.RestaurantEntry.COLUMN_NAME_MENU_URL));
        String photo_url = cursor.getString(cursor.getColumnIndexOrThrow(
                RestaurantContract.RestaurantEntry.COLUMN_NAME_PHOTO_URL));
        String rating_color = cursor.getString(cursor.getColumnIndexOrThrow(
                RestaurantContract.RestaurantEntry.COLUMN_NAME_RATING_COLOR));
        String rating_value = cursor.getString(cursor.getColumnIndexOrThrow(
                RestaurantContract.RestaurantEntry.COLUMN_NAME_RATING_VALUE));
        String upvotes = cursor.getString(cursor.getColumnIndexOrThrow(
                RestaurantContract.RestaurantEntry.COLUMN_NAME_UPVOTES));
        String online_delivery = cursor.getString(cursor.getColumnIndexOrThrow(
                RestaurantContract.RestaurantEntry.COLUMN_NAME_ONLINE_DELIVERY));
        String thumb_url = cursor.getString(cursor.getColumnIndexOrThrow(
                RestaurantContract.RestaurantEntry.COLUMN_NAME_THUMB_URL));

        RestaurantLocation rLoc = new RestaurantLocation(locality, address, latitude, longitude, city);
        UserRating userRating = new UserRating(rating_value, rating_color, upvotes);

        return new Restaurant(resId, resName, rLoc, cuisines, Double.valueOf(cost), cost_currency, thumb_url, userRating, photo_url, menu_url, feature_image, Integer.valueOf(online_delivery));
    }
}
