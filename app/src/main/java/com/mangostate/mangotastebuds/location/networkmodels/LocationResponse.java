package com.mangostate.mangotastebuds.location.networkmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class LocationResponse {
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("has_more")
    @Expose
    public Integer hasMore;
    @SerializedName("has_total")
    @Expose
    public Integer hasTotal;
    @SerializedName("location_suggestions")
    @Expose
    public List<LocationSuggestion> locationSuggestions = new ArrayList<LocationSuggestion>();

    public String getStatus() {
        return status;
    }

    public List<LocationSuggestion> getLocationSuggestions() {
        return locationSuggestions;
    }
}
