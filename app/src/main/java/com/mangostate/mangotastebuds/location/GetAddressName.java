package com.mangostate.mangotastebuds.location;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import com.mangostate.mangotastebuds.utility.PreferenceUtility;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class GetAddressName extends IntentService {
    private static final String TAG = GetAddressName.class.getSimpleName();

    public GetAddressName() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        // Address found using the Geocoder.
        List<Address> addresses = null;
        PreferenceUtility mPref = new PreferenceUtility(getApplicationContext());
        try {
            addresses = geocoder.getFromLocation(
                    Double.valueOf(mPref.getCurrentLat()),
                    Double.valueOf(mPref.getCurrentLon()),
                    // In this , we get just a single address.
                    1);
            Address address = addresses.get(0);
            if (address != null) {
                mPref.setSelectedLocation(address.getFeatureName()+","+address.getLocality() + "," + address.getAdminArea());
            }
        } catch (IOException | IllegalArgumentException ioException) {
            Log.e(TAG, ioException.getMessage());
        }
    }
}

