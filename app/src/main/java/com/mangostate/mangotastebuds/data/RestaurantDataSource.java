package com.mangostate.mangotastebuds.data;


import android.content.ContentResolver;
import android.content.ContentValues;
import android.support.annotation.NonNull;

import com.mangostate.mangotastebuds.home.networkmodels.Restaurant;

public class RestaurantDataSource {

    private static RestaurantDataSource INSTANCE;

    private ContentResolver mContentResolver;

    // Prevent direct instantiation.
    private RestaurantDataSource(@NonNull ContentResolver contentResolver) {
        mContentResolver = contentResolver;
    }

    public static RestaurantDataSource getInstance(@NonNull ContentResolver contentResolver) {
        if (INSTANCE == null) {
            INSTANCE = new RestaurantDataSource(contentResolver);
        }
        return INSTANCE;
    }

    public void saveRestaurant(@NonNull Restaurant restaurant) {
        ContentValues values = RestaurantValues.from(restaurant);
        mContentResolver.insert(RestaurantContract.RestaurantEntry.buildRestaurantsUri(), values);
    }

    public void deleteAllRestaurants() {
        mContentResolver.delete(RestaurantContract.RestaurantEntry.buildRestaurantsUri(), null, null);
    }

}

