package com.mangostate.mangotastebuds.utility;

public class AppConstants {

    public static final String KEY_RESTAURANT_ID = "restaurant_id";
    public static final int HOME_ERROR = 401;
    public static final int DETAIL_ERROR = 402;
    public static final int REVIEW_ERROR = 403;
    public static final int LOC_ERROR = 404;
}
