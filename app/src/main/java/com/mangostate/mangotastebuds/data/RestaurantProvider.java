package com.mangostate.mangotastebuds.data;


import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;

public class RestaurantProvider extends ContentProvider {

    private static final int RESTAURANT = 100;
    private static final int RESTAURANT_ITEM = 101;
    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private DbHelper mDbHelper;

    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = RestaurantContract.CONTENT_AUTHORITY;

        matcher.addURI(authority, RestaurantContract.RestaurantEntry.TABLE_NAME, RESTAURANT);
        matcher.addURI(authority, RestaurantContract.RestaurantEntry.TABLE_NAME + "/*", RESTAURANT_ITEM);

        return matcher;
    }

    @Override
    public boolean onCreate() {
        mDbHelper = new DbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case RESTAURANT:
                return RestaurantContract.CONTENT_RESTAURANT_TYPE;
            case RESTAURANT_ITEM:
                return RestaurantContract.CONTENT_RESTAURANT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor retCursor;
        switch (sUriMatcher.match(uri)) {
            case RESTAURANT:
                retCursor = mDbHelper.getReadableDatabase().query(
                        RestaurantContract.RestaurantEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            case RESTAURANT_ITEM:
                String[] where = {uri.getLastPathSegment()};
                retCursor = mDbHelper.getReadableDatabase().query(
                        RestaurantContract.RestaurantEntry.TABLE_NAME,
                        projection,
                        RestaurantContract.RestaurantEntry.COLUMN_NAME_RES_ID + " = ?",
                        where,
                        null,
                        null,
                        sortOrder
                );
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        return retCursor;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mDbHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri;

        switch (match) {
            case RESTAURANT:
                Cursor exists = db.query(
                        RestaurantContract.RestaurantEntry.TABLE_NAME,
                        new String[]{RestaurantContract.RestaurantEntry.COLUMN_NAME_RES_ID},
                        RestaurantContract.RestaurantEntry.COLUMN_NAME_RES_ID + " = ?",
                        new String[]{values.getAsString(RestaurantContract.RestaurantEntry.COLUMN_NAME_RES_ID)},
                        null,
                        null,
                        null
                );
                if (exists.moveToLast()) {
                    long _id = db.update(
                            RestaurantContract.RestaurantEntry.TABLE_NAME, values,
                            RestaurantContract.RestaurantEntry.COLUMN_NAME_RES_ID + " = ?",
                            new String[]{values.getAsString(RestaurantContract.RestaurantEntry.COLUMN_NAME_RES_ID)}
                    );
                    if (_id > 0) {
                        returnUri = RestaurantContract.RestaurantEntry.buildRestaurantUriWith(_id);
                    } else {
                        throw new android.database.SQLException("Failed to insert row into " + uri);
                    }
                } else {
                    long _id = db.insert(RestaurantContract.RestaurantEntry.TABLE_NAME, null, values);
                    if (_id > 0) {
                        returnUri = RestaurantContract.RestaurantEntry.buildRestaurantUriWith(_id);
                    } else {
                        throw new android.database.SQLException("Failed to insert row into " + uri);
                    }
                }
                exists.close();
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        return returnUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mDbHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsDeleted;

        switch (match) {
            case RESTAURANT:
                rowsDeleted = db.delete(
                        RestaurantContract.RestaurantEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mDbHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsUpdated;

        switch (match) {
            case RESTAURANT:
                rowsUpdated = db.update(RestaurantContract.RestaurantEntry.TABLE_NAME, values, selection,
                        selectionArgs
                );
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        return rowsUpdated;
    }
}

