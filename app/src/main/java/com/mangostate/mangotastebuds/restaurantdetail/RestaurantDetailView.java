package com.mangostate.mangotastebuds.restaurantdetail;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mangostate.mangotastebuds.R;
import com.mangostate.mangotastebuds.data.RestaurantContract;
import com.mangostate.mangotastebuds.data.RestaurantValues;
import com.mangostate.mangotastebuds.home.networkmodels.Restaurant;
import com.mangostate.mangotastebuds.restaurantdetail.networkmodel.Review;
import com.mangostate.mangotastebuds.restaurantdetail.networkmodel.ReviewResponse;
import com.mangostate.mangotastebuds.restaurantdetail.networkmodel.UserReview;
import com.mangostate.mangotastebuds.restaurantdetail.recycler.ReviewsAdapter;
import com.mangostate.mangotastebuds.utility.AppConstants;
import com.mangostate.mangotastebuds.utility.PreferenceUtility;
import com.mangostate.mangotastebuds.utility.ServiceGenerator;
import com.mangostate.mangotastebuds.webview.CustomTabActivityHelper;
import com.mangostate.mangotastebuds.webview.WebviewFallback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RestaurantDetailView extends AppCompatActivity implements OnMapReadyCallback, LoaderManager.LoaderCallbacks<Cursor> {
    private static final String TAG = RestaurantDetailView.class.getSimpleName();
    private static final int mReviewCountLimit = 5;
    private static final int LOADER_ID = 2;

    private Restaurant mDetailResponse;
    private RecyclerView.LayoutManager mLayoutManager;
    private ReviewResponse mReviewResponse;
    private String mResId;
    private GoogleMap mMap;
    private Double mLat, mLon;
    private PreferenceUtility mPref;
    private String mMenuURl;
    private String mPhotosUrl;
    private ReviewsAdapter mAdapter;
    private List<Review> mReviewList = new ArrayList<>();
    private RestaurantDetailAPI mDetailService = ServiceGenerator.createService(RestaurantDetailAPI.class);


    //UI Views
    private CollapsingToolbarLayout mCollapsing;
    private RecyclerView mReviewView;
    private FloatingActionButton mBack;
    private CardView mMenu, mPhotos;
    private TextView mAddress, mCost, mHomeDelivery, mCuisines, mVotes, mRating;
    private LinearLayout mDirections;
    private ProgressBar mProgressBar;
    private SupportMapFragment mMapFragment;
    private LinearLayout mMapLayout;
    private ImageView mFeaturedImage;
    private CoordinatorLayout mCoordinator;
    private Snackbar mSnackBar;
    private NestedScrollView mRestaurantView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_detail_restaurant);

        //Fetch Shared Preferences
        mPref = new PreferenceUtility(getApplicationContext());

        //Initialise UI
        initialiseViews();

        //Get Restaurant Id from Intent
        mResId = getIntent().getStringExtra(AppConstants.KEY_RESTAURANT_ID);

        //Initiate the Loader to get specific Restaurant Detail
        getSupportLoaderManager().initLoader(LOADER_ID, null, this);

        //Get Restaurant Reviews
        getUserReviewData();
    }

    private void initialiseViews() {

        mCollapsing = (CollapsingToolbarLayout) findViewById(R.id.detail_collapsible);
        mBack = (FloatingActionButton) findViewById(R.id.back_home);
        mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mMenu = (CardView) findViewById(R.id.menu);
        mPhotos = (CardView) findViewById(R.id.photos);
        mMapLayout = (LinearLayout) findViewById(R.id.map_layout);
        mFeaturedImage = (ImageView) findViewById(R.id.detail_restaurant_image);
        mAddress = (TextView) findViewById(R.id.address_value);
        mCost = (TextView) findViewById(R.id.detail_restaurant_cost);
        mHomeDelivery = (TextView) findViewById(R.id.detail_restaurant_online_delivery);
        mCuisines = (TextView) findViewById(R.id.cuisines_value);
        mVotes = (TextView) findViewById(R.id.upvotes);
        mRating = (TextView) findViewById(R.id.detail_restaurant_rating);
        mDirections = (LinearLayout) findViewById(R.id.get_direction);
        mCoordinator = (CoordinatorLayout) findViewById(R.id.detail_coordinator);
        mProgressBar = (ProgressBar) findViewById(R.id.detail_progress);
        mRestaurantView = (NestedScrollView) findViewById(R.id.background);
        mReviewView = (RecyclerView) findViewById(R.id.reviews_list);

        //Setting Recycler Adapter Value
        mAdapter = new ReviewsAdapter(RestaurantDetailView.this, mReviewList);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mReviewView.setLayoutManager(mLayoutManager);
        mReviewView.setItemAnimator(new DefaultItemAnimator());
        mReviewView.setAdapter(mAdapter);
        mReviewView.setNestedScrollingEnabled(false);

        //Setting Collapsible Toolbar Values
        mCollapsing.setCollapsedTitleTextColor(ContextCompat.getColor(this, R.color.white));
        mCollapsing.setExpandedTitleColor(ContextCompat.getColor(this, R.color.white));
        mCollapsing.setCollapsedTitleTypeface(Typeface.DEFAULT_BOLD);
        mMapFragment.getMapAsync(RestaurantDetailView.this);

        //On Click Listeners
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        mMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMenu();
            }
        });
        mPhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPhotos();
            }
        });
        mDirections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDirections();

            }
        });
    }

    private void showDirections() {
        if (mDetailResponse != null) {
            Uri gmmIntentUri = Uri.parse("google.navigation:q=" + mLat + "," + mLon);
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);
        } else {
            sendToast(getResources().getString(R.string.no_direction));
        }
    }

    private void showPhotos() {
        if (mDetailResponse != null) {
            CustomTabsIntent.Builder intentBuilder = new CustomTabsIntent.Builder();
            //Open the Custom Tab
            CustomTabsIntent customTabsIntent = intentBuilder.build();
            CustomTabActivityHelper.openCustomTab(
                    this, customTabsIntent, Uri.parse(mPhotosUrl), new WebviewFallback());

        } else {
            sendToast(getResources().getString(R.string.no_phots));
        }
    }

    private void showMenu() {
        if (mDetailResponse != null) {
            CustomTabsIntent.Builder intentBuilder = new CustomTabsIntent.Builder();
            //Open the Custom Tab
            CustomTabsIntent customTabsIntent = intentBuilder.build();
            CustomTabActivityHelper.openCustomTab(
                    this, customTabsIntent, Uri.parse(mMenuURl), new WebviewFallback());

        } else {
            sendToast(getResources().getString(R.string.no_menu));
        }
    }

    private void sendToast(String iToastMessage) {
        mSnackBar = Snackbar
                .make(mCoordinator, iToastMessage, Snackbar.LENGTH_LONG);
        mSnackBar.show();
    }

    private void setDetailData() {
        try {
            //Set Lat & Lon
            mLat = Double.valueOf(mDetailResponse.getLocation().getLatitude());
            mLon = Double.valueOf(mDetailResponse.getLocation().getLongitude());

            //Setting the Map
            if (mMap != null) {
                setUpMap();
            } else {
                mMapLayout.setVisibility(View.GONE);

            }

            //Setting the Title
            mCollapsing.setTitle(mDetailResponse.getName());

            //Setting the Featured Image using Picasso
            //In case no Image is provided from Server
            String url = mDetailResponse.getFeaturedImage();
            if (url == null || "".equals(url)) {
                Picasso.with(this)
                        .load(R.drawable.detail_restaurant_placeholder)
                        .into(mFeaturedImage);
            } else {
                Picasso.with(this)
                        .load(mDetailResponse.getFeaturedImage())
                        .error(R.drawable.detail_restaurant_placeholder)
                        .into(mFeaturedImage);
            }
            //Setting Text fields values
            mAddress.setText(mDetailResponse.getLocation().getAddress());
            String cost = mDetailResponse.getCurrency() + " " + mDetailResponse.getAverageCostForTwo();
            mCost.setText(cost);
            if ("1".equals(mDetailResponse.getHasOnlineDelivery())) {
                mHomeDelivery.setText(getResources().getString(R.string.available));
            } else {
                mHomeDelivery.setText(getResources().getString(R.string.not_available));
            }
            mCuisines.setText(mDetailResponse.getCuisines());
            mVotes.setText(mDetailResponse.getUserRating().getVotes());
            mRating.setText(mDetailResponse.getUserRating().getAggregateRating());
            mRating.setBackgroundColor(Color.parseColor("#" + mDetailResponse.getUserRating().getRatingColor()));
            mMenuURl = mDetailResponse.getMenuUrl();
            mPhotosUrl = mDetailResponse.getPhotosUrl();
        } catch (Exception ex) {
            Log.d(TAG, "error " + ex.toString());
            sendSnack(AppConstants.DETAIL_ERROR);
        }
        showProgress(false);
    }

    private void showProgress(final boolean inBoolShow) {
        mProgressBar.setVisibility(inBoolShow ? View.VISIBLE : View.GONE);
        mRestaurantView.setVisibility(inBoolShow ? View.GONE : View.VISIBLE);
    }

    private void sendSnack(int iCode) {
        String msg;
        switch (iCode) {
            //Detail Restaurant  Error
            case AppConstants.DETAIL_ERROR:
                msg = getString(R.string.detail_error);
                mSnackBar = Snackbar
                        .make(mCoordinator, msg, Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                getRestaurantDetailFromServer(mResId);
                            }
                        });
                mSnackBar.show();
                break;
        }
    }

    private void setReviewData() {
        try {
            for (UserReview reviewWrapper : mReviewResponse.getUserReviews()) {
                mReviewList.add(reviewWrapper.getReview());
            }
            mAdapter.notifyDataSetChanged();
            mReviewView.setVisibility(View.VISIBLE);
        } catch (Exception ex) {
            Log.d(TAG, "error " + ex.toString());
            sendSnack(AppConstants.REVIEW_ERROR);
        }
    }

    /*-----------------------------------------------------------FETCHING DATA FROM ZOMATO API'S-----------------------------------------------------------------*/

    private void getRestaurantDetailFromServer(String restaurantId) {
        showProgress(true);

        Call<Restaurant> detailCall;
        //Calling the API Call Object with Callbacks : Success & Failure
        detailCall = mDetailService.getRestaurantDetail(restaurantId);
        detailCall.enqueue(new Callback<Restaurant>() {
            @Override
            public void onResponse(Call<Restaurant> call, Response<Restaurant> response) {
                if (response.isSuccessful()) {
                    mDetailResponse = response.body();
                    setDetailData();
                } else {
                    Log.d(TAG, "error " + response.message());
                    showProgress(false);
                    sendSnack(AppConstants.DETAIL_ERROR);
                }
            }

            @Override
            public void onFailure(Call<Restaurant> call, Throwable t) {
                showProgress(false);
                Log.d(TAG, "fail error");
                sendSnack(AppConstants.DETAIL_ERROR);
            }
        });
    }

    private void getUserReviewData() {
        Call<ReviewResponse> reviewCall;
        //Calling the API Call Object with Callbacks : Success & Failure
        reviewCall = mDetailService.getReviewDetail(mResId, mReviewCountLimit);
        reviewCall.enqueue(new Callback<ReviewResponse>() {
            @Override
            public void onResponse(Call<ReviewResponse> call, Response<ReviewResponse> response) {
                if (response.isSuccessful()) {
                    mReviewResponse = response.body();
                    setReviewData();
                } else {
                    Log.d(TAG, " response error " + response.message());
                    sendSnack(AppConstants.REVIEW_ERROR);
                }
            }

            @Override
            public void onFailure(Call<ReviewResponse> call, Throwable t) {
                Log.d(TAG, "fail error");
                sendSnack(AppConstants.REVIEW_ERROR);
            }
        });
    }


    /*-----------------------------------------------------------SETTING UP THE MAP FRAGMENT-------------------------------------------------------------------------*/

    private void setUpMap() {
        try {
            LatLng loc = new LatLng(mLat, mLon);
            CameraUpdate center =
                    CameraUpdateFactory.newLatLng(loc);
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
            mMap.addMarker(new MarkerOptions()
                    .position(loc)
                    .title(mDetailResponse.getName()));
            mMap.moveCamera(center);
            mMap.animateCamera(zoom);
            mMap.getUiSettings().setAllGesturesEnabled(false);
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
            mMapLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        if (mDetailResponse != null) {
            setUpMap();
        }
    }

    /*-----------------------------------------------------------------------------CONTENT PROVIDER STARTS------------------------------------------------------------------*/

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(
                RestaurantDetailView.this,
                RestaurantContract.RestaurantEntry.buildRestaurantsUri(),
                RestaurantContract.RestaurantEntry.RESTAURANT_COLUMNS,
                RestaurantContract.RestaurantEntry.COLUMN_NAME_RES_ID + " = ?", new String[]{mResId}, null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data == null || !data.moveToFirst()) {
            getRestaurantDetailFromServer(mResId);
        } else {
            while (!data.isAfterLast()) {
                mDetailResponse = RestaurantValues.from(data);
                setDetailData();
                data.moveToNext();
            }
            showProgress(false);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
