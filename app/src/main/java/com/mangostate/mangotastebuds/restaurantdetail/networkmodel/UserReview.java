
package com.mangostate.mangotastebuds.restaurantdetail.networkmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserReview {

    @SerializedName("review")
    @Expose
    private Review review;

    public Review getReview() {
        return review;
    }
}
