package com.mangostate.mangotastebuds.home.networkmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GeoLocationResponse {
    @SerializedName("results_found")
    @Expose
    private Integer resultsFound;
    @SerializedName("results_start")
    @Expose
    private Integer resultsStart;
    @SerializedName("results_shown")
    @Expose
    private Integer resultsShown;
    @SerializedName("restaurants")
    @Expose
    private List<AllRestaurants> restaurants = new ArrayList<AllRestaurants>();

    public List<AllRestaurants> getRestaurants() {
        return restaurants;
    }

    public Integer getResultsFound() {
        return resultsFound;
    }
}


