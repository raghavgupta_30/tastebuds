package com.mangostate.mangotastebuds.data;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

import com.mangostate.mangotastebuds.BuildConfig;

public final class RestaurantContract {

    public static final String CONTENT_AUTHORITY = BuildConfig.APPLICATION_ID;
    public static final String CONTENT_RESTAURANT_TYPE = "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + RestaurantEntry.TABLE_NAME;
    public static final String CONTENT_RESTAURANT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + RestaurantEntry.TABLE_NAME;
    private static final String CONTENT_SCHEME = "content://";
    public static final Uri BASE_CONTENT_URI = Uri.parse(CONTENT_SCHEME + CONTENT_AUTHORITY);
    private static final String SEPARATOR = "/";

    public RestaurantContract() {
    }

    public static Uri getBaseTaskUri(String taskId) {
        return Uri.parse(CONTENT_SCHEME + CONTENT_RESTAURANT_ITEM_TYPE + SEPARATOR + taskId);
    }

    /* Inner class that defines the table contents */
    public static abstract class RestaurantEntry implements BaseColumns {
        public static final String TABLE_NAME = "restaurant";
        public static final String COLUMN_NAME_RES_ID = "resid";
        public static final String COLUMN_NAME_RES_NAME = "resname";
        public static final String COLUMN_NAME_ADDRESS = "address";
        public static final String COLUMN_NAME_CITY = "city";
        public static final String COLUMN_NAME_LOCALITY = "locality";
        public static final String COLUMN_NAME_LATITUDE = "latitude";
        public static final String COLUMN_NAME_LONGITUDE = "longitude";
        public static final String COLUMN_NAME_CUISINES = "cuisines";
        public static final String COLUMN_NAME_COST_CURRENCY = "currency";
        public static final String COLUMN_NAME_COST = "cost";
        public static final String COLUMN_NAME_PHOTO_URL = "photourl";
        public static final String COLUMN_NAME_MENU_URL = "menuurl";
        public static final String COLUMN_NAME_FEATURED_IMAGE_URL = "featuredimage";
        public static final String COLUMN_NAME_THUMB_URL = "thumb";
        public static final String COLUMN_NAME_ONLINE_DELIVERY = "onlinedelivery";
        public static final String COLUMN_NAME_RATING_COLOR = "ratingcolor";
        public static final String COLUMN_NAME_RATING_VALUE = "ratingvalue";
        public static final String COLUMN_NAME_UPVOTES = "upvotes";

        public static final Uri CONTENT_RESTAURANT_URI = BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

        public static String[] RESTAURANT_COLUMNS = new String[]{
                RestaurantContract.RestaurantEntry._ID,
                RestaurantEntry.COLUMN_NAME_RES_ID,
                RestaurantEntry.COLUMN_NAME_RES_NAME,
                RestaurantEntry.COLUMN_NAME_ADDRESS,
                RestaurantEntry.COLUMN_NAME_CITY,
                RestaurantEntry.COLUMN_NAME_LOCALITY,
                RestaurantEntry.COLUMN_NAME_LATITUDE,
                RestaurantEntry.COLUMN_NAME_LONGITUDE,
                RestaurantEntry.COLUMN_NAME_CUISINES,
                RestaurantEntry.COLUMN_NAME_COST_CURRENCY,
                RestaurantEntry.COLUMN_NAME_COST,
                RestaurantEntry.COLUMN_NAME_PHOTO_URL,
                RestaurantEntry.COLUMN_NAME_MENU_URL,
                RestaurantEntry.COLUMN_NAME_FEATURED_IMAGE_URL,
                RestaurantEntry.COLUMN_NAME_THUMB_URL,
                RestaurantEntry.COLUMN_NAME_ONLINE_DELIVERY,
                RestaurantEntry.COLUMN_NAME_RATING_COLOR,
                RestaurantEntry.COLUMN_NAME_RATING_VALUE,
                RestaurantEntry.COLUMN_NAME_UPVOTES
        };

        public static Uri buildRestaurantUriWith(long id) {
            return ContentUris.withAppendedId(CONTENT_RESTAURANT_URI, id);
        }

        public static Uri buildRestaurantsUri() {
            return CONTENT_RESTAURANT_URI.buildUpon().build();
        }

    }

}

