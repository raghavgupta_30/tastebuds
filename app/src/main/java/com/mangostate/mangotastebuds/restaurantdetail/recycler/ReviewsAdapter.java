package com.mangostate.mangotastebuds.restaurantdetail.recycler;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mangostate.mangotastebuds.R;
import com.mangostate.mangotastebuds.restaurantdetail.networkmodel.Review;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.MyViewHolder> {

    private List<Review> reviewsList;
    private Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView userName, userRating, userReview, userTimestamp;
        public ImageView userPhoto;
        public LinearLayout userView;

        public MyViewHolder(View view) {
            super(view);
            userName = (TextView) view.findViewById(R.id.user_name);
            userRating = (TextView) view.findViewById(R.id.user_rating);
            userReview = (TextView) view.findViewById(R.id.user_review);
            userTimestamp = (TextView) view.findViewById(R.id.timestamp);
            userPhoto = (ImageView) view.findViewById(R.id.user_photo);
            userView = (LinearLayout) view.findViewById(R.id.user_view);

        }
    }

    public ReviewsAdapter(Context context, List<Review> reviewsList) {
        mContext = context;
        this.reviewsList = reviewsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.review_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Review review = reviewsList.get(position);

        holder.userName.setText(review.getUser().getName());
        holder.userView.setBackgroundColor(Color.parseColor("#" + review.getRatingColor()));
        holder.userRating.setText(String.valueOf(review.getRating()));
        holder.userRating.setBackgroundColor(Color.parseColor("#" + review.getRatingColor()));
        holder.userReview.setText(review.getReviewText());
        holder.userTimestamp.setText(review.getReviewTimeFriendly());
        //In case no Image is provided from Server
        String url = review.getUser().getProfileImage();
        if (url == null || "".equals(url)) {
            Picasso.with(mContext)
                    .load(R.drawable.placeholder3)
                    .resize(70, 70)
                    .centerCrop()
                    .error(R.drawable.placeholder3)
                    .placeholder(R.drawable.placeholder3)
                    .into(holder.userPhoto);

        } else {
            Picasso.with(mContext)
                    .load(url)
                    .resize(70, 70)
                    .centerCrop()
                    .error(R.drawable.placeholder3)
                    .placeholder(R.drawable.placeholder3)
                    .into(holder.userPhoto);
        }
    }

    @Override
    public int getItemCount() {
        return reviewsList.size();
    }
}
