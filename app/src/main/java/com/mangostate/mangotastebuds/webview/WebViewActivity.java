package com.mangostate.mangotastebuds.webview;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.mangostate.mangotastebuds.R;

public class WebViewActivity extends AppCompatActivity {
    public static final String EXTRA_URL = "extra.url";
    private WebView mWebView;
    private FloatingActionButton mFab;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        //Initializing the Views
        initialiseViews();

        String url = getIntent().getStringExtra(EXTRA_URL);
        if (url != null) {
            mWebView.setWebViewClient(new WebViewClient());
            mWebView.getSettings().setJavaScriptEnabled(true);
            mWebView.loadUrl(url);
        }
    }

    private void initialiseViews() {
        //Setting WebView
        mWebView = (WebView) findViewById(R.id.web_view);

        //Setting Floating Button
        mFab = (FloatingActionButton) findViewById(R.id.fab);
        if (mFab != null)
            mFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

        //Setting SwipeRefreshLayout
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorPrimary, R.color.colorPrimaryDark);
            mSwipeRefreshLayout.setOnRefreshListener(
                    new SwipeRefreshLayout.OnRefreshListener() {
                        @Override
                        public void onRefresh() {
                            mWebView.reload();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mSwipeRefreshLayout.setRefreshing(false);
                                }
                            }, 2000);
                        }
                    }
            );
        }
    }

    @Override
    public boolean onKeyDown(int inKeyCode, KeyEvent inEvent) {
        // Check if the key event was the Back button and if there's history
        if ((inKeyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
            mWebView.goBack();
            return true;
        }
        // If it wasn't the Back key or there's no web page history, bubble up to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(inKeyCode, inEvent);
    }

}

