
package com.mangostate.mangotastebuds.home.networkmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserRating {

    @SerializedName("aggregate_rating")
    @Expose
    private String aggregateRating;
    @SerializedName("rating_text")
    @Expose
    private String ratingText;
    @SerializedName("rating_color")
    @Expose
    private String ratingColor;
    @SerializedName("votes")
    @Expose
    private String votes;

    public String getAggregateRating() {
        return aggregateRating;
    }

    public String getRatingColor() {
        return ratingColor;
    }

    public String getVotes() {
        return votes;
    }

    public UserRating(String aggregateRating, String ratingColor, String votes) {
        this.aggregateRating = aggregateRating;
        this.ratingColor = ratingColor;
        this.votes = votes;
    }
}

