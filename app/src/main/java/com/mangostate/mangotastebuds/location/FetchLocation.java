package com.mangostate.mangotastebuds.location;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.mangostate.mangotastebuds.R;
import com.mangostate.mangotastebuds.data.RestaurantDataSource;
import com.mangostate.mangotastebuds.home.HomeView;
import com.mangostate.mangotastebuds.location.networkmodels.LocationResponse;
import com.mangostate.mangotastebuds.location.networkmodels.LocationSuggestion;
import com.mangostate.mangotastebuds.utility.AppConstants;
import com.mangostate.mangotastebuds.utility.PreferenceUtility;
import com.mangostate.mangotastebuds.utility.ServiceGenerator;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FetchLocation extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        ActivityCompat.OnRequestPermissionsResultCallback,
        PlaceSelectionListener,
        ResultCallback<LocationSettingsResult> {

    private static final String TAG = FetchLocation.class.getSimpleName();
    private static final int REQUEST_CHECK_SETTINGS = 101;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 1000 * 60 * 5;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    private static final int REQUEST_LOCATION = 1;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private Location mCurrentLocation;
    private ArrayAdapter<LocationSuggestion> mZomatoAdapter;
    private List<LocationSuggestion> mZomatoList = new ArrayList<>();
    private LocationAPI mLocationService;
    private Call<LocationResponse> mLocationCall;
    private PlaceAutocompleteFragment mAutocompleteFragment;

    private PreferenceUtility mPref;

    //UI
    private CoordinatorLayout mCoordinator;
    private Snackbar mSnackBar;
    private LinearLayout mGetCurrentLocation;
    private EditText mZomatoLocation;
    private ListView mZomatoView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        //Shared Preferences
        mPref = new PreferenceUtility(getApplicationContext());

        //Initialising the Service
        mLocationService = ServiceGenerator.createService(LocationAPI.class);

        //Initialise UI Views
        initialiseViews();

        buildGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();
    }

    private void initialiseViews() {
        mAutocompleteFragment = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        mCoordinator = (CoordinatorLayout) findViewById(R.id.location_coordinator);
        mGetCurrentLocation = (LinearLayout) findViewById(R.id.get_current_location);
        mZomatoLocation = (EditText) findViewById(R.id.zomato_location);
        mZomatoView = (ListView) findViewById(R.id.zomato_location_view);
        mZomatoAdapter = new ArrayAdapter<LocationSuggestion>(this,
                android.R.layout.simple_list_item_1, mZomatoList);

        //ListView Initialise
        mZomatoView.setAdapter(mZomatoAdapter);
        mZomatoView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                setPrefValues(String.valueOf(mZomatoList.get(position).getLatitude()), String.valueOf(mZomatoList.get(position).getLongitude()));
                mPref.setSelectedLocation(mZomatoList.get(position).toString());
                showHomeScreen();
            }
        });

        //Text change Listener
        mZomatoLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 2) {
                    if (mLocationCall != null && mLocationCall.isExecuted()) {
                        mLocationCall.cancel();
                    }
                    getDataFromZomato(charSequence.toString());
                } else {
                    //Hide the List View
                    mZomatoView.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mAutocompleteFragment.setOnPlaceSelectedListener(this);
        mGetCurrentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preCheckLocationFetch();
            }
        });
    }

    /*--------------------------------------------------GET LOCATION FROM ZOMATO API'S--------------------------------------------------------------------*/
    private void getDataFromZomato(String query) {
        //Calling the API Call Object with Callbacks : Success & Failure
        mLocationCall = mLocationService.getLocations(query);
        mLocationCall.enqueue(new Callback<LocationResponse>() {
            @Override
            public void onResponse(Call<LocationResponse> call, Response<LocationResponse> response) {
                if (response.isSuccessful()) {
                    LocationResponse locationResponse = response.body();
                    if ("success".equals(locationResponse.getStatus())) {
                        setList(locationResponse.getLocationSuggestions());
                    } else {
                        mZomatoAdapter.clear();
                    }
                } else {
                    Log.d(TAG, "error " + response.message());
                }
            }

            @Override
            public void onFailure(Call<LocationResponse> call, Throwable t) {
                Log.d(TAG, "error " + t.getMessage());
            }
        });
    }

    private void setList(List<LocationSuggestion> locationSuggestions) {
        mZomatoList = locationSuggestions;
        mZomatoAdapter.clear();
        mZomatoAdapter.addAll(mZomatoList);
        mZomatoView.setVisibility(View.VISIBLE);
    }

    /*--------------------------------------------------INITIALIZING FOR FETCHING CURRENT LOCATION---------------------------------------------------------------------*/
    //Builds a GoogleApiClient
    private synchronized void buildGoogleApiClient() {
        Log.d(TAG, "GoogleApiClient building");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    //To check if a device has the needed location settings.
    private void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }


    /*--------------------------------------------------------------FOR FETCHING CURRENT LOCATION---------------------------------------------------------------------*/
    private void preCheckLocationFetch() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }

    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.d(TAG, "Locations Settings are Good");
                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.d(TAG, "Show the Dialog to User");
                try {
                    status.startResolutionForResult(FetchLocation.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.d(TAG, "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.d(TAG, "Dialog not Created");
                sendSnack(AppConstants.LOC_ERROR);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.d(TAG, "User agreed to make required location settings changes.");
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        sendToast(getResources().getString(R.string.enable_location));
                        Log.d(TAG, "User chose not to make required location settings changes.");
                        break;
                }
                break;
        }
    }

    //Requests location updates from the FusedLocationApi.
    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestLocationPermission();
        } else {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mCurrentLocation == null) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            } else {
                handleNewLocation();
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        handleNewLocation();
    }

    private void handleNewLocation() {
        stopLocationUpdates();
        setPrefValues(String.valueOf(mCurrentLocation.getLatitude()), String.valueOf(mCurrentLocation.getLongitude()));
        mPref.setSelectedLocation("");
        startService(new Intent(FetchLocation.this, GetAddressName.class));
        showHomeScreen();
    }


    private void showHomeScreen() {
        //To clear old data of Restaurants
        RestaurantDataSource ds = RestaurantDataSource.getInstance(getContentResolver());
        ds.deleteAllRestaurants();
        mPref.setDoSearch(true);
        if (mPref.isFirst()) {
            mPref.setIsFirst(false);
            Intent iHome = new Intent(FetchLocation.this, HomeView.class);
            startActivity(iHome);
            finish();

        } else {
            finish();
        }
    }

    private void setPrefValues(String lat, String lon) {
        mPref.setCurrentLat(lat);
        mPref.setCurrentLon(lon);
        mPref.setIsLocation(true);
    }

    //Removes location updates from the FusedLocationApi.
    private void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient,
                FetchLocation.this
        );
    }

    private void requestLocationPermission() {
        Log.d(TAG, "Requesting permission");
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                REQUEST_LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationUpdates();
                } else {
                    sendToast(getResources().getString(R.string.enter_loc_urself));
                }
            }
        }
    }
     /*-----------------------------------------------------------FETCHING CURRENT LOCATION ENDS------------------------------------------------------------------*/

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();

    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();

    }

    private void sendToast(String iToastMessage) {
        mSnackBar = Snackbar
                .make(mCoordinator, iToastMessage, Snackbar.LENGTH_SHORT);
        mSnackBar.show();
    }

    private void sendSnack(int iCode) {
        String msg;
        switch (iCode) {
            case AppConstants.LOC_ERROR:
                msg = getString(R.string.home_error);
                mSnackBar = Snackbar
                        .make(mCoordinator, msg, Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //Retry for Location
                                preCheckLocationFetch();
                            }
                        });
                mSnackBar.show();
                break;
        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.d(TAG, "Connected to GoogleApiClient");
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.d(TAG, "Connection suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.d(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    @Override
    public void onPlaceSelected(Place place) {
        Log.i(TAG, "Place Selected: " + place.getName());
        //Saving Values in Shared Preferences
        setPrefValues(String.valueOf(place.getLatLng().latitude), String.valueOf(place.getLatLng().longitude));
        mPref.setSelectedLocation((String) place.getName());
        showHomeScreen();
    }

    @Override
    public void onError(Status status) {

    }
}

