package com.mangostate.mangotastebuds.location.networkmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LocationSuggestion {
    @SerializedName("entity_type")
    @Expose
    public String entityType;
    @SerializedName("entity_id")
    @Expose
    public Integer entityId;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("latitude")
    @Expose
    public Double latitude;
    @SerializedName("longitude")
    @Expose
    public Double longitude;
    @SerializedName("city_id")
    @Expose
    public Integer cityId;
    @SerializedName("city_name")
    @Expose
    public String cityName;
    @SerializedName("country_id")
    @Expose
    public Integer countryId;
    @SerializedName("country_name")
    @Expose
    public String countryName;

    public String getTitle() {
        return title;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public String getCountryName() {
        return countryName;
    }

    @Override
    public String toString() {
        return getTitle() + ", " + getCountryName();
    }
}
