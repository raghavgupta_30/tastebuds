package com.mangostate.mangotastebuds.utility;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Class holding all utilities related to Shared Preferences
 */
public class PreferenceUtility {
    // Shared Preferences
    private final SharedPreferences mPref;

    // Editor for Shared preferences
    private final SharedPreferences.Editor mEditor;

    // Shared preferences file name
    private static final String PREF_NAME = "com.mangostate.mangotastebuds";

    // All Shared Preferences Keys
    private static final String KEY_SELECTED_LOCATION = "selectedLocation";
    private static final String KEY_CURRENT_LAT = "currentLat";
    private static final String KEY_CURRENT_LON = "currentLon";
    private static final String KEY_IS_LOCATION = "isLocation";
    private static final String KEY_DO_SEARCH = "doSearch";
    private static final String KEY_IS_FIRST = "isFirst";

    public PreferenceUtility(Context context) {
        int PRIVATE_MODE = 0;
        mPref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        mEditor = mPref.edit();
    }

    public void setIsLocation(boolean isFirst) {
        mEditor.putBoolean(KEY_IS_LOCATION, isFirst);
        mEditor.commit();
    }

    public boolean isLocation() {
        return mPref.getBoolean(KEY_IS_LOCATION, false);
    }

    public void setIsFirst(boolean isFirst) {
        mEditor.putBoolean(KEY_IS_FIRST, isFirst);
        mEditor.commit();
    }

    public boolean isFirst() {
        return mPref.getBoolean(KEY_IS_FIRST, true);
    }


    public void setDoSearch(boolean isFirst) {
        mEditor.putBoolean(KEY_DO_SEARCH, isFirst);
        mEditor.commit();
    }

    public boolean isDoSearch() {
        return mPref.getBoolean(KEY_DO_SEARCH, false);
    }
    public void setSelectedLocation(String selectedLocation) {
        mEditor.putString(KEY_SELECTED_LOCATION, selectedLocation);
        mEditor.commit();
    }

    public String getSelectedLocation() {
        return mPref.getString(KEY_SELECTED_LOCATION, null);
    }

    public void setCurrentLat(String lat) {
        mEditor.putString(KEY_CURRENT_LAT, lat);
        mEditor.commit();
    }

    public String getCurrentLat() {
        return mPref.getString(KEY_CURRENT_LAT, "0");
    }

    public void setCurrentLon(String lon) {
        mEditor.putString(KEY_CURRENT_LON, lon);
        mEditor.commit();
    }

    public String getCurrentLon() {
        return mPref.getString(KEY_CURRENT_LON, "0");
    }


}


