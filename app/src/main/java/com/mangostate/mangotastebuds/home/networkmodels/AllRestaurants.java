
package com.mangostate.mangotastebuds.home.networkmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AllRestaurants {
    @SerializedName("restaurant")
    @Expose
    private Restaurant restaurant;

    public Restaurant getRestaurant() {
        return restaurant;
    }

}
